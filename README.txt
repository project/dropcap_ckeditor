CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers
 * Links


INTRODUCTION
------------

This module gives the popup field formatter for the image field types.


REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/2960545 for further information.

 * Or You can install it using composer,
   $ composer require drupal/dropcap_ckeditor

 * You can also install it using drupal console cli,
   $ drupal module:install dropcap_ckeditor


CONFIGURATION
-------------

 * HOME >> Administration >> Configuration >> Content authoring >> Text formats and
    editors
   - In the above path, we have the image drop cap icon in available buttons, drag into active toolbar.


MAINTAINERS
-----------

 * Revathi.B   - https://www.drupal.org/u/revathib


LINKS
-----

Project page: https://www.drupal.org/project/dropcap_ckeditor
Documentation: https://www.drupal.org/node/2960545
Submit bug reports, feature suggestions: https://www.drupal.org/project/issues/dropcap_ckeditor